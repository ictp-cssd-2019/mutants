import numpy as np

class InvalidSequenceError(RuntimeError):
    pass

class Gene:    
    '''
    Class Gene represents a gene in the mutation analyser.
    '''
    def __init__(self, name, sequence):
        '''
        This function takes the name and the sequence of the gene as strings and then 
        creates a gene object with the given name and sequence.
        '''
        self.name = name
        self._validate_sequence(sequence)
        self.sequence = sequence.lower()
        self.octamers = []
        self.z_scores = []
        self.calculate_octamers()
    
    @property
    def length(self):
        '''
        This class returns the length of the gene sequence. 
        '''
        return len(self.sequence)

    def _validate_sequence(self, sequence):
        '''
        It checks if a letter different to A, C, G, T is present in the sequence,
        then, it raises a ValueError.
        '''
        for letter in sequence:
            if not letter in 'atgc':
                raise InvalidSequenceError('Invalid letter "{l}" found in sequence "{s}"'.format(l=letter, s=sequence))              
	
    
    def calculate_octamers(self):
        '''
        This function divides the gene sequence in its corresponding octamers,
        and save them in the octamers list.
        '''        
        for i in range(self.length - 7):
            kmer = self.sequence[i:i+8]
            self.octamers.append(kmer)
        
    def calculate_z_scores(self, octamers_dictionary):
        '''
        This function takes a dictionary with the diferent combinations of octamers
        and p-index, i-index tuples and returns the z-score as the lower 
        absolute value of both of them for all the octamers in the 
        gene sequence. 
        '''        
        try:
            _tuples = [octamers_dictionary[octamer] for octamer in 
self.octamers]
            _p_list = [tup[0] for tup in _tuples]
            _i_list = [tup[1] for tup in _tuples]
            for p,i in zip(_p_list, _i_list):
                if abs(p) < abs(i):
                    self.z_scores.append(p)
                else:
                    self.z_scores.append(i)
        except KeyError:
            print("That's not a valid base letter! Take off your gloves! ;)")
        
      

