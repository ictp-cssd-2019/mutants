Tutorial
==========

Basic steps to run the Mutation analyser

1. In console mode type the command: ./mutate

 The program will start, and you will obtain the Main Window as shown in Fig. 1. 

  .. image:: ./main_window.png
   :width: 600
 



   
2. Click on the base pair you would like to mutate and then type the new base pair using the keyboard. The program will display a vertical line in the place of the mutation, the changes in the z_score values in red, and the base pair of the mutation, as shown in Fig. 2.

  .. image:: ./mutant.png
   :width: 600
   



3. You can perform multiple mutations in the same plot. For that, just click on the base pair that you want to change. It is also possible to change a mutation that you have already performed by clicking in the corresponding base pair. See Fig. 3 and Fig. 4.

  .. image:: ./mutant_same_base.png
   :width: 600

  .. image:: ./multiple_mutant.png
   :width: 600
 


4. It is also possible to make a zoom in the z_scores plot to have a better view of some specific region, as shown in Fig. 5.

  .. image:: ./zoom.png
   :width: 600


5. To save the current image of the mutations type letter s and save the image in the desired location. See Fig. 6. 

  .. image:: ./save_image.png
   :width: 600





