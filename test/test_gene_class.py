import sys
from os.path import join, dirname
sys.path.append(join(dirname(__file__), ".."))
import gene_class
import numpy as np

gene = gene_class.Gene("ASD", "ATAACGAGAC".lower())
test_dict = {"ATAACGAG".lower():(0.2237,0.8591), 
"TAACGAGA".lower():(1.5195,0.6782),  
"AACGAGAC".lower():(1.9926,1.5834)}
test_zscores = [0.2237,0.6782, 1.5834]

def test_gene_constructor():
    """
    Test constructor of class Gene
    """
    assert gene.name == "ASD"
    assert gene.sequence == "ATAACGAGAC".lower()
    
def test_length():
    '''
    Test length function of the Gene class.
    '''
    assert gene.length == 10
   
def test_caculate_octamers():
    '''
    Test to check calculate_octamers function.
    '''
    assert gene.octamers == ["ATAACGAG".lower(), "TAACGAGA".lower(), 
"AACGAGAC".lower()]

def test_calculate_z_scores():
    '''
    Test to check the calculate_z_scores function.
    '''
    gene.calculate_z_scores(test_dict)
    
    np.testing.assert_almost_equal(gene.z_scores,test_zscores) 
