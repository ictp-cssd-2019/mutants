# Mutation Analyser
This is the final group project for the **7th Workshop on Collaborative Scientific Software Development**, taking place from 29/04/19 to 10/05/19 at ICTP in Trieste, Italy. 
The main goal of the project is to have an interactive mutation analyzer software. We think that this tool would be  useful in the fields of medical or biological sciences.

It will allow scientists in the field of bioinformatics to introduce a mutation in a gene sequence and observe if the mutation is an Exonic Splicing Enhancer (ESE) or an Exonic Splicing Silencer (ESS). 


## How does it work?
The gene must be in the file gens.fasta in fasta format which is available in the directory of the project. To run the Mutation Analyser you have to run the file main_interactive.py. An interactive window should pop-up and you will be able to perform the desired mutations. If you have doubts about how to use it, you can refer to the Tutorial Webpage: https://ictp-cssd-2019.gitlab.io/mutants/index.html

