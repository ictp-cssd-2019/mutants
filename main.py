#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Main file for mutation plotter

Created on Tue May  7 11:42:49 2019
"""
from gene_class import Gene
import numpy as np
import matplotlib.pyplot as plt
plt.style.use("seaborn-white")

def read_dictionary():
    '''
    This function takes a file with three columns (octamers, p-index, i-index) 
    that must be named "octamers.txt" and returns a dictionary with the 
    octamer has the key and a tuple (p-index, i-ndex) as the value.
    '''
    octamers=list(np.loadtxt("./octamers.txt",dtype=str,skiprows=24,usecols=0))
    p_index=list(np.loadtxt("./octamers.txt",skiprows=24,usecols=1))
    i_index=list(np.loadtxt("./octamers.txt",skiprows=24,usecols=2))
    tuples_p_i=[(p,i) for (p,i) in zip (p_index,i_index)]
    dic={octamers:key_values for (octamers,key_values) in zip (octamers,tuples_p_i)}
    
    return dic

def plot_z_scores(ax, text, z_scores, name, x_pos, y_pos):
    '''
    This function plots z-score vs sequence.
    '''
    
    ltext = len(text)
    ax.set_ylim([-8, 8])
    ax.set_xlim([0, ltext*2+2])
    text = text.upper()
    #ax.text(x_pos, y_pos, text, fontsize = 8)
    lseq = len(z_scores)
    lseq*=2
    
    x = [i for i in range(0,lseq,2)]
    plt.xticks(x,text,fontsize=8)
    #plt.xticks([])
    plt.suptitle('Mutation analysis',fontsize=20)
    plt.ylabel('Chasin z-score')
    
    ax.plot(x,z_scores, label = name)
    
def read_fasta_file(filename):
    '''
    This function takes the filename of the fasta file and returns a list 
    with Gene objects. 
    '''
    
    gene_list = []
    with open(filename) as input_file:
        for line in input_file:
            line = line.lower()
            line = line.strip('\n')
            pos = line.find('>')
            if pos !=-1:
                len_line = len(line)
                gene_name = line[pos+1 : len_line]
            else:
                if len(line)>1:
                    gene_sequence = line
                else:
                    gene_sequence = ''
            
                gen = Gene(gene_name, gene_sequence)
                gene_list.append(gen)
    
    return gene_list
    

'''
Main function
'''
def main():
    
    print('Mutation analyser')
    
    dic = read_dictionary()
    
    fasta_file = './gens.fasta'
    gene_list = read_fasta_file(fasta_file)
    
    # Calculate z_score for the genes
    for gene in gene_list:
        gene.calculate_z_scores(dic)
    
    fig, ax = plt.subplots() #if we use  plt.subplots(figsize=(float,float)) we can set the initia
    #size in wich the interactive windows its going to open.
    x_pos = 8
    y_pos = -7.4

    #Creating the genes     
    for gene in gene_list:
        text = gene.sequence
        name = gene.name
    
    #Plotting the wild type
    x_pos = 8
    y_pos = -7.8
    gene = gene_list[0]
    text = gene.sequence
    name = gene.name
    z_scores = gene.z_scores + [0,0,0,0,0,0,0]
    plot_z_scores(ax, text, z_scores, name, x_pos, y_pos)
    
    ax.legend(loc = 'best')
    plt.axhspan(-2.62,2.62 , color='yellow', alpha=0.5)

    #set horizontal black line
    plt.axhline(y=0.0,color='gray',linestyle='--')

    #set vertical black line randomly
    #xposition = [5,36,50]
    #for xc in xposition:
    #    plt.axvline(x=xc, color='gray', linestyle='--')

    plt.show()
    
    
#Initializing the Mutation Analyser

main()

    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    




